# 微同软件管理系统

## 介绍

### 服务器推荐配置
| 名称        | CPU    |  内存  |  硬盘  |  价格  |
| --------    | -----:   | :----: | :----: | :----: |
| 服务器(标准型S2机型 5M 双机)       | 2核      |   8G    |   50G    |   2970元/三年    |
| MySQL高可用版| 1核      |   1G    |   100G    | 423元/三年    |

| COS资源包     | 大小     |  价格     |
| --------     | -----:   | :----:   |
| 标准型存储容量 |200GB	  |1年	171元|
| 下行流量      | 500GB	  |3个月	177元|

[抢购地址](https://curl.qcloud.com/7X1upulR)

### 项目说明
- platform-base是一个轻量级的，前后端分离的Java快速开发平台
- JavaDoc：https://apidoc.gitee.com/fuyang_lipengjun/platform-base/

### 优势
- 严格遵循阿里编码规约开发，便于阅读及二次开发
- 实现前后端分离，通过token进行数据交互，前端再也不用关注后端技术
- 灵活的权限控制，可控制到页面和按钮，满足绝大部分的权限需求
- 可在线生成vue、controller、entity、xml、dao、service、vue、sql代码，增删改查代码一键生成，减少80%以上的开发任务
- 引入quartz定时任务，可动态完成任务的添加、修改、删除、暂停、恢复及日志查看等功能
- 引入API模板，根据token作为登录令牌，极大的方便了APP接口开发
- 引入Hibernate Validator校验框架，轻松实现后端校验
- 引入云存储服务，已支持：七牛云、阿里云、腾讯云、本地存储
- 自定义实现swagger文档支持，方便编写API接口文档
- 使用Mybatis拦截器实现数据权限，对代码侵入小
- 接口支持微信开放平台、微信公众平台、微信小程序、微信公众号开发
- 移动端已支持微信小程序登录、微信公众号登录、支付宝小程序登录

### 技术选型：
```
- Spring Boot 2.7.3
- Apache Shiro 1.4.0
- MyBatis-Plus 3.5.2
- weixin-java-mp 4.3.9.B
- weixin-java-miniapp 4.3.9.B
- weixin-java-pay 4.3.9.B
- weixin-java-open 4.3.9.B
- alipay-sdk 3.7.110.ALL
- Quartz 2.3.0
- Druid 1.2.9
- lombok 1.18.4
- swagger 2.9.2
- jwt 0.9.1
- easypoi 4.0.0
```

### 项目结构
```
platform-base
├─sql  项目SQL语句
│
├─platform-admin 管理后台(port:8888)
│
├─platform-admin-ui 管理后台UI
│ 
├─platform-api 接口服务(port:8889)
│ 
├─platform-biz 业务、数据处理
│ 
└─platform-common 公共类

```

### 实现功能
```
- 系统管理
    - 菜单管理
    - 组织机构
    - 系统参数
    - 字典管理
    - 文件上传
    - 短信配置
    - 定时任务
    - 系统日志
- 权限管理
    - 管理员列表
    - 角色管理
- 开发工具
    - 令牌管理
    - 缓存信息
    - SQL监控
    - 接口文档
    - 代码生成器
- 公众号管理
    - 公众号消息
    - 公众号菜单
    - 消息模板
    - 消息模版日志
    - 素材管理
    - 草稿箱
    - 发布记录
    - 自动回复规则
    - 带参二维码
- 用户管理
    - 用户管理
```

**效果图：**
- 菜单管理
  ![https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/platform-plus.jpg](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/platform-plus.jpg "菜单管理")
- 字典管理
  ![https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/dict.png](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/dict.png "字典管理")
- 在线人数
  ![https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/users.png](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/users.png "在线人数")
- 缓存数据
  ![https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/doc.png](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/redis.png "缓存数据")
- 接口文档
  ![https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/doc.png](https://platform-wxmall.oss-cn-beijing.aliyuncs.com/upload/platform-plus/doc.png "接口文档")

### 后端部署
- 通过git下载源码
- 创建数据库plaftorm-plus
- mysql执行`_sql/base.sql`文件，初始化数据
- 修改admin、api模块下`application-dev.yml`，修改MySQL账号和密码
- 运行PlatformAdminApplication.java启动后台管理接口服务

       -接口：http://localhost:8888/platform-admin
- 运行PlatformApiApplication.java启动api接口服务

        -接口：http://localhost:8889/platform-api

- Swagger路径：http://localhost:8889/platform-api/doc.html

### 提交反馈
1. 欢迎提交 issue，请写清楚遇到问题的原因，开发环境，复显步骤。

2. 官方QQ群：
- <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=ac742b7481b95fac926a3f2196085108bceeebcdf14bd716cbea519751e69445"><img border="0" src="http://pub.idqqimg.com/wpa/images/group.png" alt="微同软件 ①群" title="微同软件 ①群"></a>
- <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=dcb460bfa21213a2712677bab7292fd8eb2138a1914af5af397b58e7c02690c5"><img border="0" src="http://pub.idqqimg.com/wpa/images/group.png" alt="微同软件 ②群" title="微同软件 ②群"></a>
- <a target="_blank" href="//shang.qq.com/wpa/qunwpa?idkey=c7657db5d4e65a40e46ee5998534f7b9b9fa56d0347c3af3157c7f6240c0d0dd"><img border="0" src="http://pub.idqqimg.com/wpa/images/group.png" alt="微同软件 ③群" title="微同软件 ③群"></a>

### 环境推荐
- 服务器环境 
    - CentOS
    - Nginx
    - JDK1.8
    - MySQL5.7+
    - Redis
- 开发工具
    - IntelliJ IDEA、Eclipse
    - JetBrains WebStorm
    - Navicat for MySQL
    - Xshell
    - Xftp
    - Postman

### 生产环境打包
- 打包
    - mvn clean
    - mvn package -P prod
- 将platform-admin.jar、platform-api.jar上传至服务器
- 启动服务
    ##### 在服务器编写脚本
    > vim startPlatformAdminService.sh
    ~~~bash
    (netstat -tlnp|grep 8890|awk '{print $7}'|awk -F '/' '{print $1}') |xargs kill -9 
    echo "................kill the process platform-admin.............................."
    nohup java -jar platform-admin.jar &
    sleep 5s
    tail -f /home/logs/platform-admin/info.log
    ~~~
    > vim startPlatformApiService.sh
    ~~~bash
    (netstat -tlnp|grep 8889|awk '{print $7}'|awk -F '/' '{print $1}') |xargs kill -9 
    echo "................kill the process platform-api.............................."
    nohup java -jar platform-api.jar &
    sleep 5s
    tail -f /home/logs/platform-api/info.log
    ~~~
    ##### 执行这两个脚本，启动服务

### 前端启动
- 请看：[./platform-admin-ui/README.md](./platform-admin-ui/README.md)

### 后台管理系统默认密码
- admin admin

### 常用API
- [Mybatis-Plus](https://baomidou.gitee.io/mybatis-plus-doc/#/quick-start)
- [Vue](https://cn.vuejs.org/v2/api/)
- [element-ui](http://element-cn.eleme.io/#/zh-CN/component/installation)
- [echarts](https://www.echartsjs.com/api.html#echarts)
- [iconfont](https://www.iconfont.cn/search/index)
