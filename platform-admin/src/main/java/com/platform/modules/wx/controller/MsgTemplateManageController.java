/*
 *
 *      Copyright (c) 2018-2099, lipengjun All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the fly2you.cn developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: lipengjun (939961241@qq.com)
 *
 */
package com.platform.modules.wx.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.platform.common.utils.RestResponse;
import com.platform.modules.wx.entity.MsgTemplateEntity;
import com.platform.modules.wx.form.TemplateMsgBatchForm;
import com.platform.modules.wx.service.MsgTemplateService;
import com.platform.modules.wx.service.TemplateMsgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;

/**
 * 消息模板
 *
 * @author 李鹏军
 */
@RestController
@RequestMapping("/manage/msgTemplate")
@Api(tags = {"消息模板-管理后台", "模板消息的模板"})
public class MsgTemplateManageController {
    @Autowired
    private MsgTemplateService msgTemplateService;
    @Autowired
    private TemplateMsgService templateMsgService;
    @Autowired
    private WxMpService wxMpService;

    /**
     * 列表
     */
    @GetMapping("/list")
    @RequiresPermissions("wx:msgtemplate:list")
    @ApiOperation(value = "列表")
    public RestResponse list(@RequestParam Map<String, Object> params) {
        IPage page = msgTemplateService.queryPage(params);

        return RestResponse.success().put("page", page);
    }

    /**
     * 信息
     */
    @GetMapping("/info/{id}")
    @RequiresPermissions("wx:msgtemplate:info")
    @ApiOperation(value = "详情-通过ID")
    public RestResponse info(@PathVariable("id") String id) {
        MsgTemplateEntity msgTemplate = msgTemplateService.getById(id);

        return RestResponse.success().put("msgTemplate", msgTemplate);
    }

    /**
     * 信息
     */
    @GetMapping("/getByName")
    @RequiresPermissions("wx:msgtemplate:info")
    @ApiOperation(value = "详情-通过名称")
    public RestResponse getByName(String name) {
        MsgTemplateEntity msgTemplate = msgTemplateService.selectByName(name);

        return RestResponse.success().put("msgTemplate", msgTemplate);
    }

    /**
     * 保存
     */
    @PostMapping("/save")
    @RequiresPermissions("wx:msgtemplate:save")
    @ApiOperation(value = "保存")
    public RestResponse save(@RequestBody MsgTemplateEntity msgTemplate) {
        msgTemplateService.save(msgTemplate);

        return RestResponse.success();
    }

    /**
     * 修改
     */
    @PostMapping("/update")
    @RequiresPermissions("wx:msgtemplate:update")
    @ApiOperation(value = "修改")
    public RestResponse update(@RequestBody MsgTemplateEntity msgTemplate) {
        msgTemplateService.updateById(msgTemplate);

        return RestResponse.success();
    }

    /**
     * 删除
     */
    @PostMapping("/delete")
    @RequiresPermissions("wx:msgtemplate:delete")
    @ApiOperation(value = "删除")
    public RestResponse delete(@RequestBody String[] ids) {
        msgTemplateService.removeByIds(Arrays.asList(ids));

        return RestResponse.success();
    }

    /**
     * 同步公众号模板
     */
    @PostMapping("/syncWxTemplate")
    @RequiresPermissions("wx:msgtemplate:save")
    @ApiOperation(value = "同步公众号模板")
    public RestResponse syncWxTemplate() throws WxErrorException {
        msgTemplateService.syncWxTemplate();
        return RestResponse.success();
    }

    /**
     * 批量向用户发送模板消息
     * 通过用户筛选条件（一般使用标签筛选），将消息发送给数据库中所有符合筛选条件的用户
     */
    @PostMapping("/sendMsgBatch")
    @RequiresPermissions("wx:msgtemplate:save")
    @ApiOperation(value = "批量向用户发送模板消息", notes = "将消息发送给数据库中所有符合筛选条件的用户")
    public RestResponse sendMsgBatch(@RequestBody TemplateMsgBatchForm form) {
        templateMsgService.sendMsgBatch(form);
        return RestResponse.success();
    }
}
